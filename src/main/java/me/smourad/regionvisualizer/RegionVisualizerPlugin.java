package me.smourad.regionvisualizer;

import org.bukkit.plugin.java.JavaPlugin;

public final class RegionVisualizerPlugin extends JavaPlugin {

    private static RegionVisualizerPlugin instance;

    private RegionVisualizer regionVisualizer;

    @Override
    public void onEnable() {
        instance = this;

        regionVisualizer = new RegionVisualizerImpl();
        getServer().getPluginManager().registerEvents(regionVisualizer, this);
    }

    @Override
    public void onDisable() {
        regionVisualizer.clear();
    }

    public RegionVisualizer getRegionVisualizer() {
        return regionVisualizer;
    }

    public static RegionVisualizerPlugin getInstance() {
        return instance;
    }

}
