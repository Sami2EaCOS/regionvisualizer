package me.smourad.regionvisualizer;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import org.bukkit.*;
import org.bukkit.entity.BlockDisplay;
import org.bukkit.entity.Display;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.util.BoundingBox;
import org.bukkit.util.Transformation;
import org.bukkit.util.Vector;
import org.joml.AxisAngle4f;
import org.joml.Vector3f;

import java.util.*;

public class RegionVisualizerImpl implements RegionVisualizer {

    private final Map<ProtectedRegion, Map<Player, List<BlockDisplay>>> boundaries = new HashMap<>();
    private final Set<Player> omniscient = new HashSet<>();

    private final Random random;

    public RegionVisualizerImpl() {
        random = new Random();
    }

    @Override
    public void clear() {
        omniscient.clear();
        boundaries.values().stream()
                .flatMap(entry -> entry.values().stream())
                .flatMap(Collection::stream)
                .forEach(BlockDisplay::remove);
        boundaries.clear();
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent event) {
        hide(event.getPlayer());
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        if (!omniscient.contains(player)) return;

        showRegions(player);
    }

    public void showRegions(Player player) {
        int viewDistance = Bukkit.getViewDistance() * 16;
        BoundingBox area = BoundingBox.of(player.getLocation(), viewDistance, viewDistance, viewDistance);

        World world = player.getWorld();
        com.sk89q.worldedit.world.World w = BukkitAdapter.adapt(world);

        RegionManager regionManager = WorldGuard.getInstance().getPlatform().getRegionContainer().get(w);
        assert regionManager != null;

        regionManager.getRegions()
                .values().stream()
                .filter(region -> area.overlaps(getRegionBoundingBox(region)))
                .forEach(region -> {
                    if (!boundaries.getOrDefault(region, new HashMap<>()).containsKey(player)) {
                        show(player, region);
                    }
                });
    }

    @Override
    public void show(Player player) {
        omniscient.add(player);
        showRegions(player);
    }

    @Override
    public void show(Player player, ProtectedRegion region) {
        show(player, region, getRandomColor());
    }

    protected Color getRandomColor() {
        int r = random.nextInt(256);
        int g = random.nextInt(256);
        int b = random.nextInt(256);
        return Color.fromRGB(r, g, b);
    }

    protected BoundingBox getRegionBoundingBox(ProtectedRegion region) {
        BlockVector3 minimum = region.getMinimumPoint();
        BlockVector3 maximum = region.getMaximumPoint();

        return BoundingBox.of(
                new Vector(minimum.getX(), minimum.getY(), minimum.getZ()),
                new Vector(maximum.getX(), maximum.getY() + 1, maximum.getZ())
        );
    }

    @Override
    public void show(Player player, ProtectedRegion region, Color color) {
        World world = player.getWorld();
        BoundingBox bb = getRegionBoundingBox(region);

        double x1 = bb.getMinX();
        double y1 = bb.getMinY();
        double z1 = bb.getMinZ();
        double x2 = bb.getMaxX();
        double y2 = bb.getMaxY();
        double z2 = bb.getMaxZ();
        Vector center = bb.getCenter();

        boundaries.putIfAbsent(region, new HashMap<>());
        Map<Player, List<BlockDisplay>> regionBoundaries = boundaries.get(region);
        if (regionBoundaries.containsKey(player)) {
            regionBoundaries.get(player).forEach(boundary -> boundary.setGlowColorOverride(color));
            return;
        }

        List<BlockDisplay> playerRegionBoundaries = List.of(
                createBoundary(world, center, new Vector(x1, y1, z1), new Vector(x1, y1, z2), color),
                createBoundary(world, center, new Vector(x1, y1, z2), new Vector(x1, y2, z2), color),
                createBoundary(world, center, new Vector(x1, y2, z2), new Vector(x1, y2, z1), color),
                createBoundary(world, center, new Vector(x1, y2, z1), new Vector(x1, y1, z1), color),

                createBoundary(world, center, new Vector(x2, y1, z1), new Vector(x2, y1, z2), color),
                createBoundary(world, center, new Vector(x2, y1, z2), new Vector(x2, y2, z2), color),
                createBoundary(world, center, new Vector(x2, y2, z2), new Vector(x2, y2, z1), color),
                createBoundary(world, center, new Vector(x2, y2, z1), new Vector(x2, y1, z1), color),

                createBoundary(world, center, new Vector(x1, y1, z1), new Vector(x2, y1, z1), color),
                createBoundary(world, center, new Vector(x1, y1, z2), new Vector(x2, y1, z2), color),
                createBoundary(world, center, new Vector(x1, y2, z1), new Vector(x2, y2, z1), color),
                createBoundary(world, center, new Vector(x1, y2, z2), new Vector(x2, y2, z2), color)
        );

        regionBoundaries.put(player, playerRegionBoundaries);

        for (BlockDisplay display : playerRegionBoundaries) {
            for (Player p : Bukkit.getOnlinePlayers()) {
                if (!Objects.equals(p, player)) {
                    p.hideEntity(RegionVisualizerPlugin.getInstance(), display);
                }
            }
        }
    }

    protected BlockDisplay createBoundary(World world, Vector center, Vector v1, Vector v2, Color color) {
        float size = 0.2f;
        double distance = v1.distance(v2) + size;
        Vector ratio = v2.clone().subtract(v1).normalize();
        Vector translation = center.clone().subtract(v1)
                .add(center.clone().subtract(v2));
        BlockDisplay boundary = (BlockDisplay) world.spawnEntity(new Location(world, v1.getX(), v1.getY(), v1.getZ()), EntityType.BLOCK_DISPLAY);
        boundary.setGlowing(true);
        boundary.setGlowColorOverride(color);
        boundary.setBlock(Material.WHITE_CONCRETE.createBlockData());
        boundary.setShadowStrength(0);

        float tx = translation.getX() > 0 ? -1 : 0;
        float ty = translation.getY() > 0 ? -1 : 0;
        float tz = translation.getZ() > 0 ? -1 : 0;

        Transformation transformation = new Transformation(
                new Vector3f(
                        (float) (Math.min(0, ratio.getX()) * distance + tx * size),
                        (float) (Math.min(0, ratio.getY()) * distance + ty * size),
                        (float) (Math.min(0, ratio.getZ()) * distance + tz * size)
                ),
                new AxisAngle4f(),
                new Vector3f(
                        (float) Math.max(size, Math.abs(ratio.getX()) * distance),
                        (float) Math.max(size, Math.abs(ratio.getY()) * distance),
                        (float) Math.max(size, Math.abs(ratio.getZ()) * distance)
                ),
                new AxisAngle4f()
        );
        boundary.setTransformation(transformation);

        return boundary;
    }

    @Override
    public void hide(Player player, ProtectedRegion region) {
        List<BlockDisplay> display = boundaries
                .getOrDefault(region, new HashMap<>())
                .remove(player);

        if (Objects.nonNull(display)) {
            display.forEach(BlockDisplay::remove);
        }

        omniscient.remove(player);
    }

    @Override
    public void hide(Player player) {
        boundaries.keySet().forEach(region -> hide(player, region));
        omniscient.remove(player);
    }

}
