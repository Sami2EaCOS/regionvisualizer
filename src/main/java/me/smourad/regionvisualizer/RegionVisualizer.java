package me.smourad.regionvisualizer;

import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import org.bukkit.Color;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

public interface RegionVisualizer extends Listener {

    void show(Player player);
    void show(Player player, ProtectedRegion region);
    void show(Player player, ProtectedRegion region, Color color);
    void hide(Player player, ProtectedRegion region);
    void hide(Player player);
    void clear();

}
